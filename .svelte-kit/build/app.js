import { respond } from '@sveltejs/kit/ssr';
import root from './generated/root.svelte';
import { set_paths } from './runtime/paths.js';
import { set_prerendering } from './runtime/env.js';
import * as user_hooks from "./hooks.js";

const template = ({ head, body }) => "<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\t<head>\r\n\t\t<meta charset=\"utf-8\" />\r\n\t\t<link rel=\"icon\" href=\"/favicon.ico\" />\r\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\r\n\t\t<link rel=\"stylesheet\" href=\"/global.css\">\r\n\t\t" + head + "\r\n\t</head>\r\n\t<body>\r\n\t\t<div id=\"svelte\">" + body + "</div>\r\n\t</body>\r\n</html>\r\n";

let options = null;

// allow paths to be overridden in svelte-kit preview
// and in prerendering
export function init(settings) {
	set_paths(settings.paths);
	set_prerendering(settings.prerendering || false);

	options = {
		amp: false,
		dev: false,
		entry: {
			file: "/./_app/start-7faca2e6.js",
			css: ["/./_app/assets/start-a8cd1609.css"],
			js: ["/./_app/start-7faca2e6.js","/./_app/chunks/vendor-580750e4.js"]
		},
		fetched: undefined,
		floc: false,
		get_component_path: id => "/./_app/" + entry_lookup[id],
		get_stack: error => String(error), // for security
		handle_error: error => {
			console.error(error.stack);
			error.stack = options.get_stack(error);
		},
		hooks: get_hooks(user_hooks),
		hydrate: true,
		initiator: undefined,
		load_component,
		manifest,
		paths: settings.paths,
		read: settings.read,
		root,
		router: true,
		ssr: true,
		target: null,
		template,
		trailing_slash: "never"
	};
}

const d = decodeURIComponent;
const empty = () => ({});

const manifest = {
	assets: [{"file":"global.css","size":259,"type":"text/css"}],
	layout: "src/routes/__layout.svelte",
	error: ".svelte-kit/build/components/error.svelte",
	routes: [
		{
						type: 'endpoint',
						pattern: /^\/index\.json$/,
						params: empty,
						load: () => import("..\\..\\src\\routes\\index.json.js")
					},
		{
						type: 'page',
						pattern: /^\/$/,
						params: empty,
						a: ["src/routes/__layout.svelte", "src/routes/index.svelte"],
						b: [".svelte-kit/build/components/error.svelte"]
					},
		{
						type: 'endpoint',
						pattern: /^\/posts\/([^/]+?)\.json$/,
						params: (m) => ({ url: d(m[1])}),
						load: () => import("..\\..\\src\\routes\\posts\\[url].json.js")
					},
		{
						type: 'page',
						pattern: /^\/posts\/([^/]+?)\/?$/,
						params: (m) => ({ url: d(m[1])}),
						a: ["src/routes/__layout.svelte", "src/routes/posts/[url].svelte"],
						b: [".svelte-kit/build/components/error.svelte"]
					}
	]
};

// this looks redundant, but the indirection allows us to access
// named imports without triggering Rollup's missing import detection
const get_hooks = hooks => ({
	getSession: hooks.getSession || (() => ({})),
	handle: hooks.handle || (({ request, render }) => render(request))
});

const module_lookup = {
	"src/routes/__layout.svelte": () => import("..\\..\\src\\routes\\__layout.svelte"),".svelte-kit/build/components/error.svelte": () => import("./components\\error.svelte"),"src/routes/index.svelte": () => import("..\\..\\src\\routes\\index.svelte"),"src/routes/posts/[url].svelte": () => import("..\\..\\src\\routes\\posts\\[url].svelte")
};

const metadata_lookup = {"src/routes/__layout.svelte":{"entry":"/./_app/pages/__layout.svelte-ed53f37d.js","css":["/./_app/assets/pages/__layout.svelte-24bf3c71.css"],"js":["/./_app/pages/__layout.svelte-ed53f37d.js","/./_app/chunks/vendor-580750e4.js"],"styles":null},".svelte-kit/build/components/error.svelte":{"entry":"/./_app/error.svelte-d3042682.js","css":[],"js":["/./_app/error.svelte-d3042682.js","/./_app/chunks/vendor-580750e4.js"],"styles":null},"src/routes/index.svelte":{"entry":"/./_app/pages/index.svelte-59b45ae4.js","css":["/./_app/assets/pages/index.svelte-38581971.css"],"js":["/./_app/pages/index.svelte-59b45ae4.js","/./_app/chunks/vendor-580750e4.js"],"styles":null},"src/routes/posts/[url].svelte":{"entry":"/./_app/pages/posts/[url].svelte-dfe4ba48.js","css":["/./_app/assets/pages/posts/[url].svelte-37bdd981.css"],"js":["/./_app/pages/posts/[url].svelte-dfe4ba48.js","/./_app/chunks/vendor-580750e4.js"],"styles":null}};

async function load_component(file) {
	return {
		module: await module_lookup[file](),
		...metadata_lookup[file]
	};
}

init({ paths: {"base":"","assets":"/."} });

export function render(request, {
	prerender
} = {}) {
	const host = request.headers["host"];
	return respond({ ...request, host }, options, { prerender });
}