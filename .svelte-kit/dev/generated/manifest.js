const c = [
	() => import("..\\..\\..\\src\\routes\\__layout.svelte"),
	() => import("..\\components\\error.svelte"),
	() => import("..\\..\\..\\src\\routes\\index.svelte"),
	() => import("..\\..\\..\\src\\routes\\posts\\[url].svelte")
];

const d = decodeURIComponent;

export const routes = [
	// src/routes/index.json.js
	[/^\/index\.json$/],

	// src/routes/index.svelte
	[/^\/$/, [c[0], c[2]], [c[1]]],

	// src/routes/posts/[url].json.js
	[/^\/posts\/([^/]+?)\.json$/],

	// src/routes/posts/[url].svelte
	[/^\/posts\/([^/]+?)\/?$/, [c[0], c[3]], [c[1]], (m) => ({ url: d(m[1])})]
];

export const fallback = [c[0](), c[1]()];