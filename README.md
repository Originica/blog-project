## Simple SvelteKit Blog
### Olivia Monica - 1906350641

This project is made temporarily for PPL Individual Review blog. I'm planning to build a more scalable personal blog for a lot of different topics that also serves as a portfolio. 

Copyright: The template for this project can be accessed [here](https://github.com/programonaut/guide-blog) and the guide on [__programonaut.com__](https://www.programonaut.com/how-to-create-a-blog-with-svelte-step-by-step/).

